## Konsep Dasar Radiasi Pengion 

# Pendahuluan 

# Fundamental Konstanta Fisika 

| Ukuran | Nilai dan Satuan |
| ------ | ------ |
|    Nomor Avogadro     | $N_A=6,022 x 10^{23}$       |
|    Kecepatan Cahaya pada ruangan Vakum    | $c= 3 x 10^8 m/s$       |
|    Massa Elektron Bermuatan  | $e = 1.6 x 10^{19}As$  |
|    Masa elektron rest         | $m_e = 0.511 MeV/c^2$    |
|    Masa Proton rest           | $m_p = 938.2 MeV/c^2$     |
|    Masa Neutron               | $m_n = 939.3 MeV/c^2$     |
|    Satuan mass Unit           | $u = 931.5 MeV/c^2$      |   

# Konstanta turunan Fisika 

# Satuan Fisika 
- Nilai kuantitas fisika adalah nilai numerik (besarnya) dan satuan yang terkait
- Symbol : kuantitas fisika ditulis dengan tulisan miring sedangkan symbol dengan tulisan biasa.
contoh : $m$ = 21 kg ; $E$ = 15 M_eV
- Nilai Numerik : adalah satuan fisika yang memiliki kuantitas dan diberi spasi. 
Contoh :  21 kg bukan 21kg; 15 M_ev dan bukan 15M_eV
- Perhitungan metric satuan metric ini dikenal dengan Systéme International d’Unités (International system of units) atau SI (Satuan International)
- SI memiliki 7 besaran pokok diantanranya : 

| Besaran  | Satuan SI |
| ------ | ------ |
|   Panjang ($l$)     | meter (m)       |
|      mass $m$  | kilogram (kg)       |
|   waktu $t$     | sekon (s)       |
|      Arus Listrik $I$  | ampere(A)       |
|   Temperature($T$)     | kelvin (K)       |
|      Jumlah Mol $m$  | mole(mol)       |
|   Intensitas Cahaya ($l$)     | candela (cd)       |

# klasifikasi Fundamental Fisika Partikel

Dua kelas Fundamental partikel di kenal dengan Quarks dan Leptons 
- Quarks adalah partikel yang memiliki interaksi yang kuat. 
Quark adalah konstituen hadron dengan muatan listrik fraksional
(2/3 atau -1/3) dan dicirikan oleh salah satu dari tiga jenis muatan kuat yang disebut warna (merah, biru, hijau).
- Leptons adalah partikel ynag tidak memiliki interaski yang cukup kuat seperti : Elektron, Muon, tau, dan beberapa yang berhubungan dengan neutrons

# klasifikasi Radiasi

Radiasi di klasifikasikan menjadi 2 kategori : 
- non-ionisasi radiasi (tidak meng-ionisasi materi)
- ionisasi radiasi ( dapat meng-ionisasi materi)
    - Ionisasi langsung  (partikel bermuatan) contoh : electron, proton, partikel alpha, ion bermuatan
    - Ionisasi radiasi tidak langsung (neutral partikel) contoh : Photon ( x-ray, gamma ray, neutron)

# Klasifikasi ionisasi radiasi photon  
Ionisasi radiasi photon di klasisifikasikan menjadi 4 kategori : 
- Karakteristik x-ray 
    - Hasil dari electronic transisi diantara kulit atom 
- Bremstahlung 
    - Hasil dari elektron-nukleus interkasi Coulumb
- Sinar Gamma 
    - Hasil dari transisi nukleus
- annihilation quantum (annhiliasi radiation)
    - Hasil dari positron-eletcron anihiliasi 

# Einstein’s relativistic mass, energy, and momentum

- Masa

$m(v) = {m \over \sqrt{{1 - \Big({v \over c}}}\Big)^2} = {m_0 \over \sqrt {1-\beta^2}} = \gamma m_0$

- massa normaslisasi : 

$m {m (v) \over m_0} = {1 \over \sqrt{{1 - \Big({v \over c}}}\Big)^2} = {m_0 \over \sqrt {1-\beta^2}} = \gamma$













