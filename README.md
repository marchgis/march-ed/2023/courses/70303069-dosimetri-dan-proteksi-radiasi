# 70301020 - Dosimetri Proteksi Radiasi

## Learning Outcome Outline
- 							

## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
1 | Mampu menjelaskan konsep dasar Radiasi | [Materi](https://gitlab.com/marchgis/march-ed/2023/courses/70303069-dosimetri-dan-proteksi-radiasi/-/blob/main/Konsep%20Dasar%20Radiasi%20Pengion/radiasi-pengion.md)
2 | Mampu menjelaskan dan memahami konsep dasar radiasi pengion | [Materi](https://gitlab.com/marchgis/march-ed/2023/courses/70303069-dosimetri-dan-proteksi-radiasi/-/blob/main/Konsep%20Dasar%20Radiasi%20Pengion/radiasi-pengion.md)
3 | Efek Radiasi Pengion Terhadap Jaringan |[Materi](https://youtu.be/PPBk15qJhkw)
4 | Efek Kesehatan Radiasi (Intro Proteksi Radiasi)|[Materi](https://youtu.be/2bylk5ct3Oc)
5 | Prinsip proteksi radiasi dan Prinsip Proteksi Radiasi Eksternal| [Materi 1](https://youtu.be/J3HmmN_2rzQ) [Materi 2](https://youtu.be/Ko4KezPsN5Q)
6 | |
7 | |
 | UTS |
8 | |
9 | |
10 | |
12 | |
13 | |
14 | |
 | UAS |

## Tools


## Referensi



